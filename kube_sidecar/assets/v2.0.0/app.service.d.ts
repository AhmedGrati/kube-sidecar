export declare class AppService {
    getHello(): string;
    getTest(): string;
    getVersion(): string;
}
