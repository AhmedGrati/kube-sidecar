import os
import aiofiles
import aiohttp
from fastapi import Depends


from kube_sidecar.services.aiohttp_client import SingletonAiohttp

chunk_size = 1024


class HttpFileClient():

    def __init__(self, httpClient: aiohttp.ClientSession = Depends(SingletonAiohttp.get_aiohttp_client)):
        self.httpClient = httpClient

    async def download(self, url, path, *, headers={}):
        async with self.httpClient.get(url, headers=headers) as response:
            if response.status != 200:
                raise Exception("Cannot download file")
            if "content-size" in response.headers:
                content_size = int(response.headers["content-size"])
            elif "Content-Length" in response.headers:
                content_size = int(response.headers["Content-Length"])
            else:
                content_size = None
            try:
                async with aiofiles.open(path, 'wb') as afp:
                    total = 0
                    while True:
                        chunk = await response.content.read(chunk_size)

                        total += len(chunk)

                        if not chunk:
                            break
                        await afp.write(chunk)
                    return await response.release()
            except:
                os.remove(path)
