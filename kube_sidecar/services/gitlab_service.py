import uuid

import tarfile
import os

import aiohttp
from fastapi import Depends
import logging
from kube_sidecar.schemas.release_payload import Release
from kube_sidecar.services.aiohttp_client import SingletonAiohttp
from kube_sidecar.services.download_client import HttpFileClient
from kube_sidecar.utils.config import gitlab_config
import shutil

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

"""
This method filter list of releases. We should get the latest version major of any release.
Example: Input: [v2.0.0, v1.0.13, v1.0.12, ....... v1.0.0]  
         Output: [v2.0.0, v1.0.13]
         v1 , v1.0.13
         Release v11.0 
         v{0-9}+(.{0-9}+){0,2}
"""


def get_releases_to_update(releases):
    res = [releases[0]]
    for release in releases:
        last_minor_version = res[len(res) - 1].name.split(' ')[1].split('.')[0]
        current_minor_version = release.name.split(' ')[1].split('.')[0]
        if last_minor_version != current_minor_version:
            res.append(release)
    return res


class GitLabService:
    GITLAB_BASE_URL = "https://gitlab.com/api/v4/projects"

    def __init__(self, http_file_client: HttpFileClient = Depends(),
                 httpClient: aiohttp.ClientSession = Depends(SingletonAiohttp.get_aiohttp_client)):
        self.http_file_client = http_file_client
        self.httpClient = httpClient

    async def update_release_assets(self, release: Release):
        link = None
        for link in release.assets.links:
            if link.name == "build":
                break

        path = "/tmp/" + str(uuid.uuid4())
        logger.info(f"Start downloading {link.url}")
        await self.http_file_client.download(link.url, path,
                                             headers={'PRIVATE-TOKEN': gitlab_config.GITLAB_PRIVATE_TOKEN})
        logger.info(f"Finish downloading {link.url}")
        logger.info(f"Start opening and extracting {link.url}")
        tar = tarfile.open(path, "r:gz")
        tar.extractall(path=gitlab_config.ASSETS_DIRECTORY)
        logger.info(f"Finish opening and extracting {link.url}")
        tar.close()
        os.remove(path)

    async def sync_all_static_assets(self):
        """
         Get All Projects.
        """
        async with self.httpClient.get(url=f'{self.GITLAB_BASE_URL}?owned=true',
                                       headers={'PRIVATE-TOKEN': gitlab_config.GITLAB_PRIVATE_TOKEN}) \
                as get_project_request:
            projects = await get_project_request.json()

            """
            Filter Projects.
            """
            projects_to_eliminate = gitlab_config.STATIC_PACKAGES
            filtered_projects = [project for project in projects
                                 if project['path_with_namespace'] not in projects_to_eliminate]
            projects_id = [str(project['id']) for project in filtered_projects]
            """
            For each project we will download their specific releases.
            """

            for project_id in projects_id:
                async with self.httpClient.get(url=f'{self.GITLAB_BASE_URL}/{project_id}/releases',
                                               headers={
                                                   'PRIVATE-TOKEN': gitlab_config.GITLAB_PRIVATE_TOKEN}) \
                        as get_releases_request:
                    releases_dict = await get_releases_request.json()
                    releases = [Release(**user) for user in releases_dict]
                    """
                        if releases is not empty we should get the needed releases.
                    """
                    if releases:
                        releases_to_update = get_releases_to_update(releases)
                        for release in releases_to_update:
                            await self.update_release_assets(release)