from fastapi import FastAPI, Depends
from starlette.background import BackgroundTasks

from kube_sidecar.schemas.release_payload import Release
from kube_sidecar.services.gitlab_service import GitLabService
from prometheus_fastapi_instrumentator import Instrumentator
from kube_sidecar.instrumentation.instrumentator import http_requested_languages_total


gitlab_app = FastAPI()
instrumentator = Instrumentator()
instrumentator.instrument(gitlab_app).expose(gitlab_app).add(http_requested_languages_total())


@gitlab_app.post("/sync_static_files")
async def sync_static_files(release: Release, background_tasks: BackgroundTasks,
                            gitlab_service: GitLabService = Depends()):
    background_tasks.add_task(gitlab_service.update_release_assets, release)
    return True


@gitlab_app.get("/sync_all")
async def sync_all(gitlab_service: GitLabService = Depends()):
    await gitlab_service.sync_all_static_assets()
    return True
