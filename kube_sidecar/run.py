import uvicorn
if __name__ == "__main__":
    uvicorn.run("sync_static_file:gitlab_app", host="0.0.0.0", port=8000, reload=True)
