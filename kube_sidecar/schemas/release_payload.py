from __future__ import annotations

from typing import Any, List, Optional

from pydantic import BaseModel


class Project(BaseModel):
    id: int = None
    name: str = None
    description: str = None
    web_url: str = None
    avatar_url: Any = None
    git_ssh_url: str = None
    git_http_url: str = None
    namespace: str = None
    visibility_level: int = None
    path_with_namespace: str = None
    default_branch: str = None
    ci_config_path: Any = None
    homepage: str = None
    url: str = None
    ssh_url: str = None
    http_url: str = None


class Link(BaseModel):
    id: int = None
    external: bool = None
    link_type: str = None
    name: str = None
    url: str = None


class Source(BaseModel):
    format: str = None
    url: str = None


class Assets(BaseModel):
    count: int = None
    links: List[Link] = None
    sources: List[Source] = None


class Author(BaseModel):
    name: str = None
    email: str = None


class Commit(BaseModel):
    id: str = None
    message: str = None
    title: str = None
    timestamp: str = None
    url: str = None
    author: Author = None


class Release(BaseModel):
    id: Optional[int] = None
    created_at: Optional[str] = None
    description: Optional[str] = None
    name: Optional[str] = None
    released_at: Optional[str] = None
    tag: Optional[str] = None
    object_kind: Optional[str] = None
    project: Optional[Project] = None
    url: Optional[str] = None
    action: Optional[str] = None
    assets: Optional[Assets] = None
    commit: Optional[Commit] = None
