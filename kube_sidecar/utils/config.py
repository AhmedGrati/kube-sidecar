import logging
import os
from typing import List
from dotenv import load_dotenv
from pydantic import BaseSettings, validator

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
load_dotenv()


class GitLabConfig(BaseSettings):
    GITLAB_PRIVATE_TOKEN: str = None
    STATIC_PACKAGES: List[str] = []

    ASSETS_DIRECTORY: str = "./assets"

    @validator("ASSETS_DIRECTORY", pre=True)
    def check_assets_dir(cls, assets_directory: str) -> str:
        if not os.path.exists(assets_directory):
            logger.error("assets directory does not exist")
            os.makedirs(assets_directory, exist_ok=True)
        return assets_directory

    class Config:
        case_sensitive = True


if os.path.isdir('/secrets'):
    gitlab_config = GitLabConfig(_secrets_dir="/secrets")
else:
    gitlab_config = GitLabConfig()
