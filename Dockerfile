FROM python:3.8

WORKDIR /app/

# Install Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

COPY pyproject.toml poetry.lock* /app/

# Allow installing dev dependencies to run tests
RUN bash -c "poetry install --no-root --no-dev"


ENTRYPOINT ["python3"]

CMD ["/app/certificate_client/client.py"]
